"use strict"
//
//  A cat once advanced a comment.
//
//  " ''''''''''''''''''''''''''''''''''''''''''''Bad
//    Bot'''''''''''''''''''''''''''''''''''''''''''' "
//      – Winky :black_cat:
//

////////////////////////////////////////////////////////////
// DEFINITIONS
////////////////////////////////////////////////////////////
import chalk from "chalk"
import Discord from "discord.js"
import { REST } from "@discordjs/rest"
import { Routes } from "discord-api-types/v10"

// import { giphyURLs } from "./env/giphy.js"
// import { detectPolly, interactSwitch  } from "./env/personality.js"
// import { rollDice } from "./env/logic.js"

const { default: CONFIGJSON } = await import( "./config.json",{ assert: { type:"json" } } )
const { TOKEN, CLIENT_ID, BOT_RIVALID } = CONFIGJSON

const { Client, Collection, Events, GatewayIntentBits } = Discord

const client = new Discord.Client( { intents:[
  GatewayIntentBits.Guilds,
  GatewayIntentBits.GuildMembers,
  GatewayIntentBits.GuildMessages,
  GatewayIntentBits.MessageContent,
  GatewayIntentBits.DirectMessages
] } )


////////////////////////////////////////////////////////////
//// WORK
////////////////////////////////////////////////////////////
// Client on ready
client.on(Events.ClientReady, () => {
  console.log(`Logged in and ready! -> ${client.user.tag}`);
});

// // Client on message
client.on(Events.MessageCreate, message => {

  // TODO debug_polly.chatlog()
  console.log(
    "\n"                                            +
    "\n"                                            +
    "==================================================="
                                                    +"\n"
    +"Guild:      " + message.guild?.name           +"\n"
    +"Guild ID:   " + message.guild?.id             +"\n"
                                                        +
    "---------------------------------------------------"
                                                    +"\n"
    +"Username:   " + message.author.username       +"#"
                    + message.author.discriminator  +"\n"
    +"UUID:       " + message.author.id             +"\n"
                                                    +"\n"
    +"Nickname:   " + message.member?.nickname      +"\n"
    +"Channel:    " + message.channel?.name         +"\n"
    +"Message:"                                     +"\n"
                                                    +"\n"
    +" " + message.content                          +"\n"
                                                    +"\n"
    +"(charactercount#" + message.content.length+")"+"\n"
                                                    +
    "==================================================="
  )
  
  // Reactions on messages
  switch (message.content) {
    // Bad Bot
    case message.content.match(/bad bot/i)?.input:
      message.channel.send(`**NO U!** 😡`)
    break
    // Shrug
    case message.content.match(/shrug/i)?.input:
      message.channel.send('¯\\_(ツ)_/¯')
    break
    // Press F
    case message.content.match(/press f/i)?.input:
      message.react('🇫')
    break
    // What Is Love
    case message.content.match(/what is love/i)?.input:
      // const pepecry = client.emojis.get('907735826971127879')
      message.channel.send(`Baby don't hurt me!`)
    break

    default:
    break
  }

  // Commands
  switch (message.content) {

    // Invite Polly
    case message.content.match("polly!invite")?.input:
      message.channel.send('🦜 **INVITE ME TO YOUR DISCORD SERVER!!**')
      message.channel.send('👉 https://discord.com/oauth2/authorize?client_id=1050420762735022130&permissions=8&scope=bot%20applications.commands')

    // Help with Polly
    // make that it matches 1:1
    case message.content.match("polly!help")?.input:
      message.channel.send('Sorry, I can\'t help you rn… ')
    break

    default:

    break
  }


});

  // Read text and count letters
  function pollydorking(message) {
    // set letters of string message.content to lowercase
    // save in array
    // prepare text to be "rEaL dOrK"
  }

client.login(TOKEN);

